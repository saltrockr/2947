public class Test {

    public static void main(String[] args) {
      
    Stack<String> stack = new Stack();
    stack.push("1 Item");
    stack.push("2 Item");
    stack.push("3 Item");
    stack.push("4 Item");
    System.out.print("1 - 2 - 3 - 4 Pushed\n");
    String temp = stack.peek();
    System.out.print(temp+"\n");
    temp = stack.peek();
    System.out.print(temp+"\n");
    temp = stack.pop();
    System.out.print(temp+"\n");
    temp = stack.pop();
    System.out.print(temp+"\n");
    
    System.out.print("1 - 2 - 3 - 4 Added to List\n");
    
}